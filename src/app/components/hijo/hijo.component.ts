import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  @Input() texto!:string;
  @Input() texto2!:string;
  @Input() texto3!:any;
  @Input() elemento!:any;


  constructor() { }

  ngOnInit(): void {
  }

}
