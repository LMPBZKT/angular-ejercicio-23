import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  padretexto = "ve al colegio y comprate almuerzo"
  textopadre2 = "hiciste lo que te dije?"

  lista = {
    Material: 'Cuadernos, Anillados, Lapizeros',

  }

  compras = {
    cuadernos: '50 hojas, 30 lapizeros, 2 anillados',
    boligrafo: 'negro,azul,morado,rojo',
    mochila: 'Hummer'
  }

  constructor() {

  }

  ngOnInit(): void {
  }

}
